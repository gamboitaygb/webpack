import firebase from 'firebase/app';
import 'firebase/auth';

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyANnvm5aDuo7_eoSlmXWTbV0YXC4zaMLQQ",
    authDomain: "tutorial-de-login.firebaseapp.com",
    databaseURL: "https://tutorial-de-login.firebaseio.com",
    projectId: "tutorial-de-login",
    storageBucket: "tutorial-de-login.appspot.com",
    messagingSenderId: "43774203265"
  };
  const fire = firebase.initializeApp(config);

  export default fire;