import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/scss/bootstrap.scss';
import App from './App.jsx';

ReactDOM.render(<App />, document.getElementById('root'));
