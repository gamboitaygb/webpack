import React, { Component } from 'react';
import Navigation from './components/Navigation/Navigation.jsx';
import fire  from './config/Fire.jsx';
import Login from './components/login/Login.jsx';
import Content from './components/Content/Content.jsx';


class App extends Component {
    constructor(props)
    {
        super(props);
        this.state={
            user:{},
        };
    }


    componentDidMount()
    {
        this.authListener();
    }


    authListener() {
        fire.auth().onAuthStateChanged((user) => {
          //console.log(user);
          if (user) {
            this.setState({ user });
           // localStorage.setItem('user', user.uid);
          } else {
            this.setState({ user: null });
          //  localStorage.removeItem('user');
          }
        });
      }


    render()
    {
        return(
            <div className="App">

                {this.state.user ?
                <React.Fragment>
                    <header className="App-header">
                        <Navigation user={ this.state.user ? this.state.user : null}/>
                    </header>
                    <div className="container-fluid">
                        <Content/>
                    </div>
                </React.Fragment>
                : 
                
                <div className="container">

                    <div className="root-container">
                        <div className="box-container">
                            <Login/>
                         </div>
                    </div>
                    
                </div>
                }
            </div>
            
        )
    }
}

export default App;