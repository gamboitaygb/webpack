import React, { Component } from 'react';
import Table from '../Table/Table.jsx';

class Content extends Component 
{
    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          items: []
        };
        this.closeAlert = this.closeAlert.bind(this);
      }


    componentDidMount() {
        fetch("http://webpack.com.io:8080/server.php?a=1",
        { 
            method: 'GET',
            credentials: 'same-origin',
    
        })
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                items: result
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }

      closeAlert()
      {
        $(".alert").removeClass('show');
      }


    render()
    {
       
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="loader"></div>;
        } else {
            return (
                <div className="container">
                <div className="alert alert-success alert-dismissible fade" onClick={this.closeAlert} role="alert">
                    <span id="alert-msj"></span>
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div>
                    <Table redirections={items}/>
                </div>
            </div>
            );
        }
          
    }
}

export default Content;