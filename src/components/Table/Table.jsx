import React, { Component } from 'react';
import Tr from './component/Tr.jsx';
var contdor=0;
class Table extends Component 
{
    constructor(props) {
        super(props);
      }



      render(){
          return(
              <table className="table table-striped">
                    <tbody>
                        {this.props.redirections.map((item,index) => (
                            <Tr key={index} tr={item} className={index < 10 ? 'd-block': 'd-none' }/>
                        ))}
                    </tbody>
              </table>
          )
      }
}

export default Table;