import React, { Component } from 'react';
import Input from './input/Input.jsx';
class Td extends Component 
{
    constructor(props){
        super(props);
    }
    render(){
        if(this.props.ind==0){
            var cl = 'origen';
        }else if(this.props.ind==1){
            var cl = 'destino';
        }else{
            var cl = 'status';
        }
        return(
            <td>
                <Input value={this.props.td} classcss={cl}/>
            </td>
        )
    }
}

export default Td;