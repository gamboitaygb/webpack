import React, { Component } from 'react';
import Td from './Td.jsx';
class Tr extends Component 
{
    constructor(props){
        super(props);

        this.state = {
            success: false,
          };

        this.handeChangeValue=this.handeChangeValue.bind(this);

    }

    handeChangeValue(e)
    {

        let parameter='?a=2&';
        for(let i = 0; i< e.target.parentElement.parentNode.cells.length-1; i++) {
             parameter += e.target.parentElement.parentNode.cells[i].firstChild.className+'='+e.target.parentElement.parentNode.cells[i].firstChild.value+'&';
        }

        parameter = parameter.slice(0,-1);

        fetch("http://webpack.com.io:8080/server.php"+parameter,
        { 
            method: 'GET',
            credentials: 'same-origin',
    
        })
          .then(res => res.json())
          .then(
            (result) => {
                $('#alert-msj').html('Redirección actualizada correctamente');
                $('.alert').addClass('show');
            }
          )

    }


    render(){

        return(
            <tr className={this.props.className}>
                {this.props.tr.map((item,index) => (
                        <Td key={index} td={item} ind={index} />
                        )
                    )
        
                }
                <td><button 
                    className="btn btn-success"
                    onClick={this.handeChangeValue}
                    >Guardar</button></td>
            </tr>
        )
    }
}

export default Tr;