import React, { Component } from 'react';
//import logo from './logo.svg';
import fire  from '../../config/Fire.jsx';
class Navigation extends Component {

    constructor(props)
    {
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout()
    {
        fire.auth().signOut();
    }
    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">
                 301
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Añadir <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Descargar Todas</a>
                    </li>
                    </ul>
                    { this.props.user ? 
                    <React.Fragment>
                        <span className="badge badge-pill badge-warning">{this.props.user.email}</span>
                        <button type="button" 
                            className="btn btn-danger"
                            onClick={this.logout}    
                            >
                            Log out
                        </button>  
                     </React.Fragment>
                     : '' 
                    }
                     </div>
                </nav>
        )
    }
}

export default Navigation;