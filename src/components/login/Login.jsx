import React, { Component } from 'react';
import fire  from '../../config/Fire.jsx';

class Login extends Component{
    constructor(props){
        super(props);
        this.state={
                email : '',
                password : ''
        };
        this.submitLogin = this.submitLogin.bind(this);
        this.handleChange     = this.handleChange.bind(this);
    }


    submitLogin(e)
    {
        e.preventDefault();
        e.preventDefault();
        fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u)=>{
        }).catch((error) => {
            console.log(error);
        });
    }

    handleChange(e)
    {
        this.setState({ [e.target.name]: e.target.value });
    }

    render(){
        return(
            <div className="inner-container">
                <div className="header">
                   <p>301</p>
                </div>
                <div className="box">
        
                    <div className="input-group">
                        <label htmlFor="email">Email</label>
                        <input
                        type="text"
                        name="email"
                        onChange={this.handleChange}
                        className="login-input"
                        placeholder="Email"/>
                    </div>
            
                    <div className="input-group">
                        <label htmlFor="password">Password</label>
                        <input
                        type="password"
                        onChange={this.handleChange}
                        name="password"
                        className="login-input"
                        placeholder="Password"/>
                    </div>
            
                    <button
                        type="button"
                        className="login-btn"
                        onClick={this.submitLogin}>
                        Login
                         </button>
                </div>
            </div>
        )
    }
}



export default Login;