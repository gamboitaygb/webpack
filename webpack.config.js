const path = require('path');
const webpack = require('webpack');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    mode: "development", // "production" | "development" | "none"
    watch: true,
    entry: './src/index.jsx',
    output: {
        path: path.resolve(__dirname, 'public/assets/js'),
        filename: 'global.js',
      },

  module: {
    rules: [
        { 
          test: /\.jsx$/, 
          exclude: /node_modules/,
          use: [{
            loader: 'babel-loader',
            options: {
                cacheDirectory: true,
                babelrc: false,//true para leerlos aqui se pone flase y se descomena,
                presets: [
                    [
                        "@babel/env", 
                        {
                            "targets": {
                                'browsers': ['Chrome >=59']
                            },
                            "modules":false,
                            "loose":true
                        }
                    ],  
                    "@babel/preset-react",
                    
                ],
            }
        }
        ]
          
        },
        {
            test: /\.scss$/,
            use: [{
                loader: "style-loader"
            }, {
                loader: "css-loader"
            }, {
                loader: "sass-loader",
                options: {
                    implementation: require("sass"),
                }
            }]
        },
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
              fallback: "style-loader",
              use: "css-loader"
            })
          }
    ]
  },
  plugins: [
    new ExtractTextPlugin("global.css"),
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      })      
  ]
}