<?php

namespace clases;
define('_REDIS_SCHEME_','tcp');
define('_REDIS_SERVER_', '127.0.0.1');
define('_REDIS_PORT_',6379);
define('_REDIS_DB_','5');
define('_REDIS_HASH_','6');

class Redis {
    public $redis;
    private $db;

    public function __contruct( $db = null )
    {
        $this->db = ($db) ? $db : _REDIS_DB_;
        $this->redis = $this->getRedis();
    }

    public function getRedis()
    {
        try {
            return new \Predis\Client([
                'scheme'   => _REDIS_SCHEME_,
                'host'     => _REDIS_SERVER_,
                'port'     => _REDIS_PORT_,
                'database' => $this->db,
            ]);
        } catch (\Exception $e) {
            return null;
        }
    }
}