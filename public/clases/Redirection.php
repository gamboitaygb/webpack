<?php
namespace clases;
use clases\Redis;

class Redirection
{
    private $redis;
    public function __construct( $action = null )
    {
            $this->redis = new \Predis\Client([
                'scheme'   => 'tcp',
                'host'     => '127.0.0.1',
                'port'     => 6379,
                'database' => 5,
            ]);
    }

    public function list()
    {
    

        $arg =$this->redis->keys('*');

        $tem=array();

        foreach ($arg as $value){
            $i = $this->redis->hgetall($value);
            $tem[]=array($value,$i[0],$i[1]);
        }

        return $tem;
    }

    public function updateAjax($org,$dest,$st)
    {

        if($this->redis->exists($org)){          
            $this->redis->hmset($org,[$dest,$st]);
            return true;
        }else{
            return false;
        }
    }
}